const template = `Description

-Feature: 1. <Add the feature name here>

**Code Review Checklist -**
* [ ] Follow coding standards
* [ ] No code conflicts
* [ ] Follow modularized code pattern
* [ ] Code should be reusable\n`

let url = document.URL;

if(url.includes("/merge_requests/new")) {
    let textarea = document.querySelector("textarea");
    textarea.value = template;

    chrome.runtime.sendMessage({ status: "done" }, function (response) {
        console.log(response.farewell);
    });
}