chrome.runtime.onInstalled.addListener(() => {
    console.log("Extension Installed");
})

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        console.log(sender.tab ?
            "from a content script:" + sender.tab.url :
            "from the extension");

        if (request.status === "done") {
            sendResponse({ farewell: "Description Added" });
        }
            
        chrome.action.setTitle({tabId: sender.tab.id, title: "Description Added"}, () => {
            console.log("Title updated");
        });

        chrome.action.setIcon({path: "./icons/check_green.png", tabId: sender.tab.id}, () => {
            console.log("Image updated");
        })
    }
);